sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History"
], function (Controller, History) {
	"use strict";

	return Controller.extend("com.inoksan.zin_qm_prj_record_result_usage_desicion.BaseController", {});

});