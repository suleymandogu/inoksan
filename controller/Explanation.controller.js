sap.ui.define(["com/inoksan/zin_qm_prj_record_result_usage_desicion/controller/BaseController",
	"sap/m/MessageBox",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function (BaseController, MessageBox, Filter, FilterOperator) {
	"use strict";
	return BaseController.extend("com.inoksan.zin_qm_prj_record_result_usage_desicion.controller.Explanation", {
		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.inoksan.zin_qm_prj_record_result_usage_desicion.view.Explanation
		 */ //	onInit: function() {
		//
		//	},
		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf com.inoksan.zin_qm_prj_record_result_usage_desicion.view.Explanation
		 */ //	onBeforeRendering: function() {
		//
		//	},
		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.inoksan.zin_qm_prj_record_result_usage_desicion.view.Explanation
		 */ //	onAfterRendering: function() {
		//
		//	},
		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.inoksan.zin_qm_prj_record_result_usage_desicion.view.Explanation
		 */ //	onExit: function() {
		//
		//	}
		/**
		 *@memberOf com.inoksan.zin_qm_prj_record_result_usage_desicion.controller.Explanation
		 */
		onInit: function () {
			var oView = this.getView();

			oView.byId("tbl_Explanation").setVisible(true);
			oView.byId("tbl_Explanation").setVisibleRowCount(1);
			oView.addStyleClass(this.getOwnerComponent().getContentDensityClass());

			this.getOwnerComponent().getRouter().getRoute("Explanation").attachPatternMatched(this._onRouteMatched, this);
		},
		_onRouteMatched: function (oEvent) {
			var oView = this.getView();
			var jsonModel = this.getOwnerComponent().getModel("jsonMain");
			var SelInspPath = jsonModel.getProperty(jsonModel.getProperty("/SelectedInsplotSPath"));
			var model = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/sap/ZIN_QM_FIORI_SERVICE_001_SRV/", true);
			model.read("/Notes_of_InsplotSet", {
				filters: [new Filter({
						path: "Order",
						operator: FilterOperator.EQ,
						value1: SelInspPath.Order
					}),
					new Filter({
						path: "OrdItem",
						operator: FilterOperator.EQ,
						value1: SelInspPath.OrdItem
					})
				],
				success: function (oData) {
					jsonModel.setProperty("/Notes_of_InsplotSet", oData.results);
					//MessageBox.success(oData.results.length.toString());
					oView.byId("tbl_Explanation").setVisible(oData.results.length > 0);
					oView.byId("tbl_Explanation").clearSelection();
					if (oData.results.length > 0) {
						oView.byId("tbl_Explanation").setVisibleRowCount(oData.results.length);
					} else {
						oView.byId("tbl_Explanation").setVisible(true);
						oView.byId("tbl_Explanation").setVisibleRowCount(1);
					}
				}
			});

		},
		action: function (oEvent) {
			var that = this;
			var actionParameters = JSON.parse(oEvent.getSource().data("wiring").replace(/'/g, "\""));
			var eventType = oEvent.getId();
			var aTargets = actionParameters[eventType].targets || [];
			aTargets.forEach(function (oTarget) {
				var oControl = that.byId(oTarget.id);
				if (oControl) {
					var oParams = {};
					for (var prop in oTarget.parameters) {
						oParams[prop] = oEvent.getParameter(oTarget.parameters[prop]);
					}
					oControl[oTarget.action](oParams);
				}
			});
			var oNavigation = actionParameters[eventType].navigation;
			if (oNavigation) {
				var oParams = {};
				(oNavigation.keys || []).forEach(function (prop) {
					oParams[prop.name] = encodeURIComponent(JSON.stringify({
						value: oEvent.getSource().getBindingContext(oNavigation.model).getProperty(prop.name),
						type: prop.type
					}));
				});
				if (Object.getOwnPropertyNames(oParams).length !== 0) {
					this.getOwnerComponent().getRouter().navTo(oNavigation.routeName, oParams);
				} else {
					this.getOwnerComponent().getRouter().navTo(oNavigation.routeName);
				}
			}
		}
	});
});