sap.ui.define([
	"com/inoksan/zin_qm_prj_record_result_usage_desicion/controller/BaseController",
	"sap/m/MessageBox",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function (BaseController, MessageBox, Filter, FilterOperator) {
	"use strict";
	return BaseController.extend("com.inoksan.zin_qm_prj_record_result_usage_desicion.controller.Main", {
		onInit: function () {
			var oView = this.getView();
			oView.addStyleClass(this.getOwnerComponent().getContentDensityClass());
			oView.byId("tbl_Insplots").setVisible(true);
			oView.byId("tbl_Insplots").setVisibleRowCount(1);
			var that = this;
			var jsonModelMain = that.getOwnerComponent().getModel("jsonMain");
			jsonModelMain.setProperty("/SerialNumber", "");
			jsonModelMain.setProperty("/Insplots_of_SerialsSet", []);
			this._checkButtonVisible();
			this.dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "dd.MM.YYYY"
			});
			var dateFormatted = this.dateFormat.format(new Date());
			oView.byId("dp_Date").setValue(dateFormatted);
		},
 
		handleChange: function (oEvent) { 
			this.getView().byId("dp_Date").setValueState(oEvent.getParameter("valid") ? "None" : "Error");
 
		},
		//özkan test
		//özkan test 28.11 
		/** >>BARCODE  **/

		onScanForValue: function (oEvent) {
			// jQuery.sap.require("sap.ndc.BarcodeScanner");
			// var scan = sap.ndc.BarcodeScanner;
			var that = this;

			// if (!scanModel) {
			// 	sap.m.MessageToast.show("...");
			// }

			// scan.scan(
			// 	function (mResult) {
			// 		scan.closeScanDialog();
			// 		if (mResult.cancelled) {
			// 			return;
			// 		}
			// 		that.getView().byId("inp_Serial").setValue(mResult.text);
			// 		that.SearchForSerial();
			// 		// alert("We got a bar code\n" +
			// 		// 	"Result: " + mResult.text + "\n" +
			// 		// 	"Format: " + mResult.format + "\n" +
			// 		// 	"Cancelled: " + mResult.cancelled);
			// 	},
			// 	function (Error) {
			// 		scan.closeScanDialog();
			// 		sap.m.MessageBox.error("Scanning failed: " + Error);
			// 	});

			var deviceModel = this.getOwnerComponent().getModel("device");
			var phone = deviceModel.getProperty("/system/phone");
			// if (phone) {
			// 	jQuery.sap.delayedCall(30000, this, function () {
			// 	});
			// }
			if (!this._oScanDialog) {

				this._oScanDialog = new sap.m.Dialog({
					title: "Scan barcode",
					contentWidth: "640px",
					contentHeight: "480px",
					horizontalScrolling: false,
					verticalScrolling: false,
					stretchOnPhone: true,
					content: [new sap.ui.core.HTML({
						id: this.createId("scanContainer"),
						content: "<div />"
					})],
					endButton: new sap.m.Button({
						text: "Cancel",
						press: function (oEvent) {
							this._oScanDialog.close();
						}.bind(this)
					}),
					afterOpen: function () {
						this._initQuagga(this.getView().byId("scanContainer").getDomRef()).done(function () {
							// Initialisation done, start Quagga
							Quagga.start();
						}).fail(function (oError) {
							// Failed to initialise, show message and close dialog...this should not happen as we have
							// already checked for camera device ni /model/models.js and hidden the scan button if none detected
							MessageBox.error(oError.message.length ? oError.message + "!" : ("Failed to initialise Quagga with reason code " + oError.name), {
								onClose: function () {
									this._oScanDialog.close();
								}.bind(this)
							});
						}.bind(this));
					}.bind(this),
					afterClose: function () {
						// Dialog closed, stop Quagga
						Quagga.stop();
					}
				});

				this.getView().addDependent(this._oScanDialog);
			}

			this._oScanDialog.open();
		},

		_initQuagga: function (oTarget) {
			var oDeferred = jQuery.Deferred();

			// Initialise Quagga plugin - see https://serratus.github.io/quaggaJS/#configobject for details
			Quagga.init({
				inputStream: {
					type: "LiveStream",
					target: oTarget,
					constraints: {
						width: {
							min: 640
						},
						height: {
							min: 480
						},
						facingMode: "environment"
					}
				},
				locator: {
					patchSize: "medium",
					halfSample: true
				},
				/*numOfWorkers	: 2,
				frequency		: 10,
				decoder			: {
					readers 		: [{
						format			: "code_128_reader",
						config			: {}
					}]
				},*/
				numOfWorkers: (navigator.hardwareConcurrency ? navigator.hardwareConcurrency : 4),
				decoder: {
					readers: [
						"code_39_reader",
						"code_39_vin_reader"
					],
				},
				locate: true
			}, function (error) {
				if (error) {
					oDeferred.reject(error);
				} else {
					oDeferred.resolve();
				}
			});

			if (!this._oQuaggaEventHandlersAttached) {
				// Attach event handlers...

				Quagga.onProcessed(function (result) {
					var drawingCtx = Quagga.canvas.ctx.overlay,
						drawingCanvas = Quagga.canvas.dom.overlay;

					if (result) {
						// The following will attempt to draw boxes around detected barcodes
						if (result.boxes) {
							drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute("width")), parseInt(drawingCanvas.getAttribute("height")));
							result.boxes.filter(function (box) {
								return box !== result.box;
							}).forEach(function (box) {
								Quagga.ImageDebug.drawPath(box, {
									x: 0,
									y: 1
								}, drawingCtx, {
									color: "green",
									lineWidth: 2
								});
							});
						}

						if (result.box) {
							Quagga.ImageDebug.drawPath(result.box, {
								x: 0,
								y: 1
							}, drawingCtx, {
								color: "#00F",
								lineWidth: 2
							});
						}

						if (result.codeResult && result.codeResult.code) {
							Quagga.ImageDebug.drawPath(result.line, {
								x: 'x',
								y: 'y'
							}, drawingCtx, {
								color: 'red',
								lineWidth: 3
							});
						}
					}
				}.bind(this));

				Quagga.onDetected(function (result) {
					// Barcode has been detected, value will be in result.codeResult.code. If requierd, validations can be done 
					// on result.codeResult.code to ensure the correct format/type of barcode value has been picked up

					if (result.codeResult.code !== null && typeof result.codeResult.code !== "undefined") {
						if (result.codeResult.code.length === 12) { // Set barcode value in input field
							this.getView().byId("inp_Serial").setValue(result.codeResult.code);
							this.SearchForSerial();

							// Close dialog
							this._oScanDialog.close();

						}
					}

				}.bind(this));

				// Set flag so that event handlers are only attached once...
				this._oQuaggaEventHandlersAttached = true;
			}

			return oDeferred.promise();
		},
		/**	<<BARCODE  **/
		/**
		 *@memberOf com.inoksan.zin_qm_prj_record_result_usage_desicion.controller.Main
		 */
		SearchForSerial: function (oEvent) {
			var that = this;
			var oDialog = that.byId("BusyDialog");
			oDialog.open();

			//This code was generated by the layout editor.
			var oView = that.getView();
			var jsonModel = this.getOwnerComponent().getModel("jsonMain");
			jsonModel.setProperty("/Insplots_of_SerialsSet", []);
			var Serial = oView.byId("inp_Serial").getValue();
			var model = that.getOwnerComponent().getModel("aaa");
			// var text = "/Insplots_of_SerialsSet?$filter=( Sernr eq '" + Serial + "' )";
			var model = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/sap/ZIN_QM_FIORI_SERVICE_001_SRV/", true);
			jsonModel.setProperty("/IsConnected", false);
			jQuery.sap.delayedCall(10000, this, function () {
				if (jsonModel.getProperty("/IsConnected")) {
					return;
				}
				sap.m.MessageBox.warning("Talep zaman aşımına uğradı tekrar deneyiniz");
				oDialog.close();
			});
			model.read("/Insplots_of_SerialsSet", {
				filters: [new Filter({
					path: "Sernr",
					operator: FilterOperator.EQ,
					value1: Serial
				})],
				success: function (oData) {
					jsonModel.setProperty("/Insplots_of_SerialsSet", oData.results);
					//MessageBox.success(oData.results.length.toString());
					oView.byId("tbl_Insplots").setVisible(oData.results.length > 0);
					oView.byId("tbl_Insplots").clearSelection();
					if (oData.results.length > 0) {
						oView.byId("tbl_Insplots").setVisibleRowCount(oData.results.length);
					} else {
						oView.byId("tbl_Insplots").setVisible(true);
						oView.byId("tbl_Insplots").setVisibleRowCount(1);
					}
					that._checkButtonVisible();
					jsonModel.setProperty("/IsConnected", true);

					oDialog.close();
				},
				error: function (err) {
					jsonModel.setProperty("/IsConnected", true);
					oDialog.close();

					MessageBox.warning(err.responseText.split(':')[5].split('}')[0]);
				},
				failed: function (oData) {
					jsonModel.setProperty("/IsConnected", true);
					oDialog.close();
					alert("failed");
				}

			});
		},
		rowSelectionChange: function (oRow) {
			var selectedIndex = oRow.getSource().getProperty("selectedIndex");
			this._checkButtonVisible(selectedIndex);
			var jsonModel = this.getOwnerComponent().getModel("jsonMain");
			// var path = oRow.getParameter("rowContext").sPath;
			// var InsSelRowIndex = jsonModel.getProperty(path).Sernr;
			var InsSelRowIndex = oRow.getParameter("rowIndex");
			if (oRow.getParameter("rowContext") === null) {
				return;
			}
			var InsSelSPath = oRow.getParameter("rowContext").sPath;
			var SelectedInsplot = jsonModel.getProperty(InsSelSPath).Prueflos;
			jsonModel.setProperty("/SelectedInsplotRowIndex", InsSelRowIndex);
			jsonModel.setProperty("/SelectedInsplotSPath", InsSelSPath);
			jsonModel.setProperty("/SelectedPrueflos", SelectedInsplot);
		},
		_checkButtonVisible: function (selectedIndex) {
				var jsonModel = this.getOwnerComponent().getModel("jsonMain");
				var items = jsonModel.getProperty("/Insplots_of_SerialsSet");
				this.getView().byId("button_Result").setVisible(selectedIndex !== -1 && typeof selectedIndex !== "undefined" && items.length > 0);
				this.getView().byId("button_UsageDesicion").setVisible(selectedIndex !== -1 && typeof selectedIndex !== "undefined" && items.length >
					0);
				this.getView().byId("button_Plan").setVisible(selectedIndex !== -1 && typeof selectedIndex !== "undefined" && items.length > 0);
				this.getView().byId("button_Notes").setVisible(selectedIndex !== -1 && typeof selectedIndex !== "undefined" && items.length > 0);
			}
			/**
			 *@memberOf com.inoksan.zin_qm_prj_record_result_usage_desicion.controller.Main
			 */
			,
		/**
		 *@memberOf com.inoksan.zin_qm_prj_record_result_usage_desicion.controller.Main
		 */
		openPlanList: function (oEvent) {
			//This code was generated by the layout editor.
			// create popover
			if (!this._oPopover) {
				this._oPopover = sap.ui.xmlfragment("com.inoksan.zin_qm_prj_record_result_usage_desicion.Fragments.PlanList", this);
				this.getView().addDependent(this._oPopover);
			}
			// delay because addDependent will do a async rerendering and the actionSheet will immediately close without it.
			var oControl = oEvent.getSource();
			this._oPopover.openBy(oControl);
		},
		_readEntity: function (whichfunction) {
			var model = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/sap/ZIN_QM_FIORI_SERVICE_001_SRV/", true);
			var jsonModel = this.getOwnerComponent().getModel("jsonMain");
			var oView = this.getView();
			var date = oView.byId("dp_Date").getValue();
			var insplot = "";
			var inspoper = "";
			var functionname = "";
			switch (whichfunction) {
			case "Result":
				functionname = "Result";
				insplot = jsonModel.getProperty("/SelectedPrueflos");
				break;
			case "UsageDesicion":
				functionname = "UsageDesicion";
				insplot = jsonModel.getProperty("/SelectedPrueflos");

				break;
			default:
			}
			model.read("/Return_FISet(FmName='" + functionname + "',Insplot='" + insplot + "',Inspoper='" + inspoper + "',ResultDate='" +
				date +
				"')", {
					success: function (oData) {
						switch (oData.Type) {
						case "E":
							sap.m.MessageBox.error(oData.Message);
							break;
						case "S":
							sap.m.MessageBox.success(oData.Message);
							break;
						default:
						}
					},
					error: function (oData) {
						alert(oData.message);
					},
					failed: function (oData) {
						alert("İşlem başarısız");
					}
				});

			// model.read("/Return_FISet(FmName='" + fm2 + "',Insplot='" + insplot2 + "',Inspoper='" + "" + "',ResultDate='" + ResultDate + "')", {
			// 	success: function (oData) {
			// 		switch (oData.results.Type) {
			// 		case "E":
			// 			sap.m.MessageBox.error(oData.results.Message);
			// 			break;
			// 		case "S":
			// 			sap.m.MessageBox.success(oData.results.Message);
			// 			break;
			// 		default:
			// 		}
			// 	},
			// 	error: function (oData) {},
			// 	failed: function (oData) {}
			// });
		},
		// readEntity:function()
		// {
		// 	var model = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/sap/ZIN_QM_FIORI_SERVICE_001_SRV/", true); 
		// 	var par1="322";
		// 	var par2="322";
		// 	model.read("/Insplots_of_SerialsSet(IvParam1='"+par1+"',IvParam2='"+par2+"')", {
		// 		success: function (oData) {
		// 			 sap.m.MessageBox.error(oData.results[0].Message);
		// 			 sap.m.MessageBox.success(oData.results[0].Message);
		// 		},
		// 		error: function (oData) {
		// 		},
		// 		failed: function (oData) {}
		// 	});
		// },
		/**
		 *@memberOf com.inoksan.zin_qm_prj_record_result_usage_desicion.controller.Main
		 */
		action: function (oEvent) {
			var that = this;
			var actionParameters = JSON.parse(oEvent.getSource().data("wiring").replace(/'/g, "\""));
			var eventType = oEvent.getId();
			var aTargets = actionParameters[eventType].targets || [];
			aTargets.forEach(function (oTarget) {
				var oControl = that.byId(oTarget.id);
				if (oControl) {
					var oParams = {};
					for (var prop in oTarget.parameters) {
						oParams[prop] = oEvent.getParameter(oTarget.parameters[prop]);
					}
					oControl[oTarget.action](oParams);
				}
			});
			var oNavigation = actionParameters[eventType].navigation;
			if (oNavigation) {
				var oParams = {};
				(oNavigation.keys || []).forEach(function (prop) {
					oParams[prop.name] = encodeURIComponent(JSON.stringify({
						value: oEvent.getSource().getBindingContext(oNavigation.model).getProperty(prop.name),
						type: prop.type
					}));
				});
				if (Object.getOwnPropertyNames(oParams).length !== 0) {
					this.getOwnerComponent().getRouter().navTo(oNavigation.routeName, oParams);
				} else {
					var jsonModel = this.getOwnerComponent().getModel("jsonMain");
					this.getOwnerComponent().getRouter().navTo(oNavigation.routeName, {
						InsSelRowIndex: jsonModel.getProperty("/SelectedInsplotRowIndex")
					});
				}
			}
		},
		/**
		 *@memberOf com.inoksan.zin_qm_prj_record_result_usage_desicion.controller.Main
		 */
		Result: function (oEvent) {
			//This code was generated by the layout editor.
			this._readEntity("Result");

		},
		/**
		 *@memberOf com.inoksan.zin_qm_prj_record_result_usage_desicion.controller.Main
		 */
		UsageDesicion: function (oEvent) {
			//This code was generated by the layout editor.
			this._readEntity("UsageDesicion");
		}
	});
});