sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function (JSONModel, Device) {
	"use strict";

	return {

		createDeviceModel: function () {
			var oDeviceModel = new sap.ui.model.json.JSONModel(Device);
			oDeviceModel.setDefaultBindingMode("OneWay");
			//Disable the scan barcode button by default
			oDeviceModel.setProperty("/barcodeScanEnabled", false);
			if (navigator && navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
				navigator.mediaDevices.getUserMedia({
					video: true
				}).then(function (stream) {
					// device supports video, which means will enable the scan button
					oDeviceModel.setProperty("/barcodeScanEnabled", true);
				}).catch(function (err) {
					// not supported, barcodeScanEnabled already default to false
				});
			}
			return oDeviceModel;
		}

	};
});